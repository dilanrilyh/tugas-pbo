# 1. Mampu menunjukkan keseluruhan Use Case beserta ranking dari tiap Use Case dari produk digital:

| usecase | Nilai |
| ------ | ------ |
|    1. Mengisi Pulsa    |  90      |
|     2. Mengedit Pulsa   |   80     |
|     3. Menghapus Data Pulsa   |  75    |
|     4. Menampilkan Daftar Pengisian Pulsa:|   70     |


# 2. Mampu mendemonstrasikan Class Diagram dari keseluruhan Use Case produk digital
```mermaid
classDiagram
    class Pulsa {
        +id: int
        +namaPengirim: string
        +noTelpPengirim: string
        +jumlahPulsa: int
        +connection: mysqli

        +__construct(servername: string, username: string, password: string, dbname: string)
        +insertPulsaData(namaPengirim: string, noTelpPengirim: string, jumlahPulsa: int): void
        +updatePulsaData(namaPengirim: string, noTelpPengirim: string, jumlahPulsa: int, editIndex: int): void
        +getPengisianPulsa(): array
    }

    class IsiPulsaPage {
        +pulsa: Pulsa

        +__construct(pulsa: Pulsa)
        +display(): void
    }

    class EditPulsaPage {
        +pulsa: Pulsa

        +__construct(pulsa: Pulsa)
        +display(editIndex: int): void
    }

    class MainApp {
        +pulsa: Pulsa
        +isiPulsaPage: IsiPulsaPage
        +editPulsaPage: EditPulsaPage

        +__construct()
        +run(): void
    }

    Pulsa --> "1..1" mysqli
    IsiPulsaPage --> Pulsa
    EditPulsaPage --> Pulsa
    MainApp --> Pulsa
    MainApp --> IsiPulsaPage
    MainApp --> EditPulsaPage
```


# 3. Mampu menunjukkan dan menjelaskan penerapan setiap poin dari SOLID Design Principle
- Prinsip Single Responsibility (SRP): Setiap fungsi atau kelas sebaiknya memiliki satu tanggung jawab tunggal.
```
// fungsi function ($request, $response) dalam route / bertanggung jawab untuk menangani permintaan dan mengembalikan respons. 

$app->get('/', function ($request, $response) {
    $content = "<h1 class='text-center'>Selamat Datang di Website Isi Pulsa & Kuota</h1>";
    $content .= "<p class='text-center'>Silakan pilih salah satu layanan di navbar di atas.</p>";

```
- Prinsip Dependency Inversion (DIP)



```
<?php
require 'vendor/autoload.php';
//Prinsip Dependency Inversion (DIP): Dalam kode tersebut,
//terdapat dependensi pada Slim Framework dengan menggunakan \Slim\App dan Slim\Http\Response.
// Prinsip DIP mengusulkan penggunaan dependensi melalui abstraksi, bukan melalui implementasi langsung.
$app = new \Slim\App();
```



# 4. Mampu menunjukkan dan menjelaskan Design Pattern yang dipilih
MVC (Model-View-Controller) atau MVP (Model-View-Presenter) untuk mengatur pemisahan tanggung jawab antara komponen aplikasi

![Dokumentasi](Dokumentasi/mvc.png)

# 5. Mampu menunjukkan dan menjelaskan konektivitas ke database

- $host, $username, $password, $database: Variabel-variabel ini menyimpan informasi yang diperlukan untuk melakukan koneksi ke database, yaitu nama host database, username, password, dan nama database.

- mysqli_connect($host, $username, $password, $database): Fungsi ini digunakan untuk membuat koneksi ke database MySQL menggunakan MySQLi. Parameter yang diberikan adalah informasi koneksi, yaitu host, username, password, dan nama database. Fungsi ini mengembalikan objek koneksi jika koneksi berhasil, atau false jika terjadi kesalahan dalam koneksi.

- if (!$conn) { die("Koneksi database gagal: " . mysqli_connect_error()); }: Kode ini memeriksa apakah koneksi ke database berhasil atau tidak. Jika koneksi gagal, maka mysqli_connect_error() akan mengembalikan pesan error yang terjadi. Pesan error tersebut akan ditampilkan menggunakan fungsi die(), yang akan menghentikan eksekusi program dan menampilkan pesan error.

![Dokumentasi](Dokumentasi/database.png)

# 6. Mampu menunjukkan dan menjelaskan pembuatan web service dan setiap operasi CRUD nya
- web service :

Slim Framework V3

- Web Page Untuk User
![Dokumentasi](Dokumentasi/webpage.png)


- Web Service Untuk Program
![Dokumentasi](Dokumentasi/webservice.png)


**Operasi CRUD**

1. Creat

Fungsi creat ini berfungsi untuk membuat isi pulsa ke no pengirim,nama Pengirim dan jumlah pulsanya.

![Dokumentasi](Dokumentasi/create.png)


2. Read

Fungsi Read ini berfungsi untuk melihat Daftar isi pulsa ke no pengirim,nama Pengirim dan jumlah pulsanya.

![Dokumentasi](Dokumentasi/read.png)


3. Update 


Fungsi creat ini berfungsi untuk mengedit isi pulsa ke no pengirim,nama Pengirim dan jumlah pulsanya.

![Dokumentasi](Dokumentasi/update.png)

4. Delete 

Fungsi creat ini berfungsi untuk menghapus isi pulsa ke no pengirim,nama Pengirim dan jumlah pulsanya.

![Dokumentasi](Dokumentasi/delete.png)
# 7. Mampu menunjukkan dan menjelaskan Graphical User Interface dari produk digital

- Header: Bagian atas tampilan berisi logo dan judul aplikasi dan teks "Isi Pulsa Dan Kuota".

- Menu Navigasi: Menampilkan menu yang berisi tautan ke fitur-fitur utama aplikasi, seperti Isi Pulsa, Isi Kuota, Kirim Pulsa. Pengguna dapat mengakses berbagai fitur dengan mengeklik menu yang sesuai.

- Tombol Dan Tautan : Tombol Isi pulsa pada menu isi pulsa yang berfungsi untuk mengisi pulsa atau tautan menuju halaman lain.
# 8. Mampu menunjukkan dan menjelaskan HTTP connection melalui GUI produk digital

1. Mengisi form untuk mengirim permintaan pengisian kuota. Pengguna diharuskan memasukkan nama pengirim, nomor telepon pengirim, jenis kuota, dan jumlah kuota yang diinginkan. Data tersebut akan dikirim melalui HTTP POST request ketika tombol "Isi Kuota" ditekan.

2. Melihat daftar pengisian kuota yang telah dilakukan. Data pengisian kuota tersebut diambil dari database dan ditampilkan dalam bentuk tabel. Setiap baris tabel menampilkan informasi seperti nama pengirim, nomor telepon, jenis kuota, jumlah kuota, dan tombol "Edit" dan "Hapus" untuk masing-masing entri.

3. Mengedit data pengisian kuota. Ketika tombol "Edit" ditekan, pengguna akan diarahkan ke halaman "edit_kuota.php" dengan data pengisian kuota yang ingin diubah.

4. Menghapus data pengisian kuota. Ketika tombol "Hapus" ditekan, pengguna akan diminta untuk konfirmasi sebelum data pengisian kuota dihapus. Jika dikonfirmasi, data akan dihapus melalui HTTP GET request.

# 9. Mampu Mendemonstrsikan produk digitalnya kepada publik dengan cara-cara kreatif melalui video Youtube

https://youtu.be/rpsL8EUkUm0
